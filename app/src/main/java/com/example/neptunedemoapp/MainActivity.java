package com.example.neptunedemoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.pax.dal.entity.ECheckMode;
import com.pax.dal.entity.EPedKeyType;
import com.pax.dal.entity.EPedType;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button1;
    private Button button2;
    private Button button3;
    private TextView resultText;
    private CheckBox checkBox;

    public static final String PED_TYPE = "PED_TYPE";

    protected EPedType pedType;



    private byte[] byte_TMK = Convert.getInstance()
            .strToBcd("380D4A75FEDF5BC1B3799DF2583876BA", Convert.EPaddingPosition.PADDING_LEFT);
    private byte[] byte_TPK = Convert.getInstance()
            .strToBcd("4BC004313B2E392567DB73D70722A005", Convert.EPaddingPosition.PADDING_LEFT);//明文：07E6D931EA75734AA4E00483ADF2134F
    private byte[] byte_TDK = Convert.getInstance()
            .strToBcd("3D20A9A5144F2615776920EF2B8FA80D", Convert.EPaddingPosition.PADDING_LEFT);//67BC0E979825972CC729FE6246E0F7AB


    private byte[] byte_TMK_KCV = Convert.getInstance().strToBcd("5E353F55", Convert.EPaddingPosition.PADDING_LEFT);
    private byte[] byte_TPK_KCV = Convert.getInstance().strToBcd("089D0C", Convert.EPaddingPosition.PADDING_LEFT);// 502974295D2E049E
    private byte[] byte_TDK_KCV = Convert.getInstance().strToBcd("2771A3", Convert.EPaddingPosition.PADDING_LEFT);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1 = (Button) findViewById(R.id.writeKey_bt1);
        button2 = (Button) findViewById(R.id.writeKey_bt2);
        button3 = (Button) findViewById(R.id.writeKey_bt3);
        resultText = (TextView) findViewById(R.id.writeKey_result_text);
        checkBox = (CheckBox) findViewById(R.id.writeKey_checkBox);
        checkBox.setVisibility(View.VISIBLE);

        button1.setText("write TMK");
        button2.setText("write TPK");
        button3.setText("write TDK");


        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        boolean resB = false;
        String resStr = "";
        switch (v.getId()) {
            case R.id.writeKey_bt1:
                if (!checkBox.isChecked()) {
                    resB = PedTester.getInstance(pedType).writeKey(EPedKeyType.TLK, (byte) 0x00, EPedKeyType.TMK,
                            (byte) 1, byte_TMK, ECheckMode. KCV_ENCRYPT_0,byte_TMK_KCV);
                    resStr = "TMK";
                } else {
                    resB = PedTester.getInstance(pedType).writeKey(EPedKeyType.TLK, (byte) 0x00, EPedKeyType.SM4_TMK,
                            (byte) 11, byte_TMK, ECheckMode. KCV_ENCRYPT_FIX_DATA, byte_TMK_KCV);
                    resStr = "SM4_TMK";
                }

                break;
            case R.id.writeKey_bt2:
                if (!checkBox.isChecked()) {
                    resB = PedTester.getInstance(pedType).writeKey(EPedKeyType.TMK, (byte) 1, EPedKeyType.TPK, (byte) 1,
                            byte_TPK, ECheckMode.KCV_ENCRYPT_0, byte_TPK_KCV);
                    resStr = "TPK";
                } else {
                    resB = PedTester.getInstance(pedType).writeKey(EPedKeyType.SM4_TMK, (byte) 11, EPedKeyType.SM4_TPK,
                            (byte) 11, byte_TPK, ECheckMode.KCV_NONE, null);
                    resStr = "SM4_TPK";
                }
                break;
            case R.id.writeKey_bt3:

                if (!checkBox.isChecked()) {
                    if(pedType == EPedType.INTERNAL){
                        resB = PedTester.getInstance(pedType).writeKey(EPedKeyType.TMK, (byte) 1, EPedKeyType.TDK, (byte) 3,
                                byte_TDK, ECheckMode.KCV_ENCRYPT_0, byte_TDK_KCV);
                        resStr = "TDK";
                    }

                } else {
                    resB = PedTester.getInstance(pedType).writeKey(EPedKeyType.SM4_TMK, (byte) 11, EPedKeyType.SM4_TDK,
                            (byte) 13, byte_TDK, ECheckMode.KCV_NONE, null);
                    resStr = "SM4_TDK";
                }
                break;
            default:
                break;

        }

                if (resB) {
                    resultText.setText("write " + resStr + " success");
                } else {
                    resultText.setText("write " + resStr + " failed");
                }


        }
    }
