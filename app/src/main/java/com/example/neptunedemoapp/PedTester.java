package com.example.neptunedemoapp;


import com.pax.dal.IPed;

import com.pax.dal.entity.ECheckMode;
import com.pax.dal.entity.EPedKeyType;

import com.pax.dal.entity.EPedType;
import com.pax.dal.entity.EUartPort;

import com.pax.dal.exceptions.PedDevException;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class PedTester extends BaseTester {
    private static PedTester pedTester;
    private static EPedType pedType;
    public static int PEDMODE ;
    private static int pedMode ;
    private IPed ped;
    private byte[] byte_test = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    private KeyPair kp = null;
    private PedTester(EPedType type) {
        //  Log.i("Test", type.name());
        pedType = type;
        pedMode = PEDMODE;
        if (pedMode == 0) {
            ped = DemoApp.getDal().getPed(type);
        } else {
            ped = DemoApp.getDal().getPedKeyIsolation(type);
        }
        if (type == EPedType.EXTERNAL_TYPEA && !DemoApp.getDal().getCommManager().getUartPortList().contains(EUartPort.PINPAD)) {
            ped.setPort(EUartPort.COM1);
        }
         kp = getKeyPair(512);
    }

    public static PedTester getInstance(EPedType type) {
        if (pedTester == null || pedType != type || pedMode != PEDMODE) {
            pedTester = new PedTester(type);
        }
        return pedTester;
    }

    private KeyPair getKeyPair(int len) {
        KeyPairGenerator kpg = null;
        try {
            kpg = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        SecureRandom random = new SecureRandom();
        kpg.initialize(len, random);
        KeyPair kp = kpg.generateKeyPair();
        return kp;
    }

    // PED writeKey include TMK,TPK,TAK,TDk
    // ===============================================================================================================

    public boolean writeKey(EPedKeyType srcKeyType, byte srcKeyIndex, EPedKeyType destKeyType, byte destkeyIndex,
                            byte[] destKeyValue, ECheckMode checkMode, byte[] tmk_kcv) {
        try {
            ped.writeKey(srcKeyType, srcKeyIndex, destKeyType, destkeyIndex, destKeyValue, checkMode, tmk_kcv);
            logTrue("writeKey");
            return true;
        } catch (PedDevException e) {
            e.printStackTrace();
            logErr("writeKey", e.toString());
        }
        return false;
    }

    // ===================================================================================================================


    // ==============================================================================
    // 获取 TPK TMK TDK 的KCV
    public byte[] getKCV_TPK() {
        try {
            byte[] bytes_tpk = ped.getKCV(EPedKeyType.TPK, (byte) 1, (byte) 0, byte_test);
            logTrue("getKCV_TPK");
            return bytes_tpk;
        } catch (PedDevException e) {
            e.printStackTrace();
            logErr("getKCV_TPK", e.toString());
        }
        return null;
    }


    public byte[] getKCV_TMK() {
        try {
            byte[] bytes_tmk = ped.getKCV(EPedKeyType.TMK, (byte) 2, (byte) 0, byte_test);
            logTrue("getKCV_TMK");
            return bytes_tmk;
        } catch (PedDevException e) {
            e.printStackTrace();
            logErr("getKCV_TMK", e.toString());
        }
        return null;
    }


    public byte[] getKCV_TDK() {
        try {
            byte[] bytes_tdk = ped.getKCV(EPedKeyType.TDK, (byte) 3, (byte) 0, byte_test);
            logTrue("getKCV_TDK");
            return bytes_tdk;
        } catch (PedDevException e) {
            e.printStackTrace();
            logErr("getKCV_TDK", e.toString());
        }
        return null;
    }
}

