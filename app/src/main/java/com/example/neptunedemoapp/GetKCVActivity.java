package com.example.neptunedemoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pax.dal.entity.EPedType;

public class GetKCVActivity extends AppCompatActivity {

    private Button button1;
    private Button button2;
    private Button button3;
    private TextView resultText;

    public static final String PED_TYPE = "PED_TYPE";

    protected EPedType pedType;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1 = (Button) findViewById(R.id.writeKey_bt1);
        button1.setVisibility(View.GONE);
        button2 = (Button) findViewById(R.id.writeKey_bt2);
        button2.setText("getTPK  KCV");
        button3 = (Button) findViewById(R.id.writeKey_bt3);
        button3.setText("getTDK  KCV");

        resultText = (TextView) findViewById(R.id.writeKey_result_text);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] tpk = PedTester.getInstance(pedType).getKCV_TPK();
                if (tpk != null) {
                    resultText.setText(getString(R.string.ped_kcvvalue_tpk1)
                            + Convert.getInstance().bcdToStr(tpk));
                } else {
                    resultText.setText(getString(R.string.ped_kcvvalue_tpk2));
                }
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] tak = PedTester.getInstance(pedType).getKCV_TDK();
                if (tak != null) {
                    resultText.setText(getString(R.string.ped_kcvvalue_tdk1)
                            + Convert.getInstance().bcdToStr(tak));
                } else {
                    resultText.setText(getString(R.string.ped_kcvvalue_tdk2));
                }
            }
        });








    }
}